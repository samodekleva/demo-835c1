<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Sale;

class TestSalesInsert extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $sale = new Sale([
            'vehicle_id' => 666,
            'seller_id' => 666,
            'buyer_id' => 666,
            'model_id' => 666,
            'sale_date' => '2020-1-12',
            'buy_date' => '2020-1-12',
        ]);

        $sale->save();

        $this->assertDatabaseHas('sales', [
            'seller_id' => 666
        ]);
    }
}
