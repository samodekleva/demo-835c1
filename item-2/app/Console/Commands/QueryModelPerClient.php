<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Buyer;
use App\Sale;


class QueryModelPerClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:model-per-client {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query best selling model per client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $buyerId = $this->argument('id');

        $buyer = Buyer::find($buyerId);

        if (!$buyer) {
            $this->output->error('No buyer with ID '.$buyerId);
        } else {

            $carModel = Sale::selectRaw('model_id, count(id) as num')
                ->where('buyer_id', $buyerId)
                ->groupBy('model_id')
                ->orderBy('num desc')
                ->get()
                ->sortByDesc('num')
                ->first();

            $this->output->success($buyer->name . ' ' . $buyer->surname .' prefers to purchase Vehicle ID '.$carModel->model_id .' ('.$carModel->num.' purcashes in database).');
        }

    }
}
