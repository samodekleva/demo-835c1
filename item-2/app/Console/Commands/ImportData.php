<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\SaleImport;
use App\Buyer;
use App\Sale;


class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from CVS file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');
        $this->output->title('Starting import');
        (new SaleImport)->withOutput($this->output)->import($path);

        $this->output->title('Faking buyers');

        $buyers = Sale::groupBy('buyer_id')->pluck('buyer_id');
        foreach ($buyers as $id) {
            if (!Buyer::find($id)) {
                factory(Buyer::class)->make(['id' => $id])->save();
            }
        }

        $this->output->success('Import successful');

    }
}
