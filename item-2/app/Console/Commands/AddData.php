<?php

namespace App\Console\Commands;

use App\Sale;
use App\Buyer;
use Illuminate\Console\Command;

class AddData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:add {row}';
    //{vehicle_id} {--seller_id=} {--buyer_id=} {--model_id=} {--sale_date=} {--buy_date=}

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a single record (CSV)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function validate($row) {
        $row = explode(',', $row);
        if (sizeof($row) != 6) {
           return 'Invalid data.';
        }
        return $row;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $row = $this->argument('row');
        $values = $this->validate($row);
        if (is_array($values)) {
            $buyer = Buyer::find($values[2]);
            if (!$buyer) {
                factory(Buyer::class)->make(['id' => $values[2]])->save();
            }
            $sale = new Sale([
                'vehicle_id' => $values[0],
                'seller_id' => $values[1],
                'buyer_id' => $values[2],
                'model_id' => $values[3],
                'sale_date' => $values[4],
                'buy_date' => $values[5],
            ]);
            $sale->save();
            $this->output->success('Item added.');
        } else {
            $this->output->error($values);
        }
    }
}
