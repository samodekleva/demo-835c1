<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BestSeller extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:bestseller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Best selling vehicle in 3 months interval';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $results = DB::select(DB::raw("
            select median_date, sq.from_date, sq.to_date, sq.model_id, count(records.id) as total
            from (select count(id)                     as num,
                         sale_date                     as median_date,
                         model_id,
                         date(sale_date, '-1.5 month') as from_date,
                         date(sale_date, '+1.5 month') as to_date
                  from sales
                  group by sale_date, model_id) sq
                     LEFT JOIN sales records
            where records.sale_date > sq.from_date
              AND records.sale_date < sq.to_date
              AND records.model_id = sq.model_id
            GROUP BY median_date, records.model_id
            ORDER BY total desc
            LIMIT 1
        "));
        if (!sizeof($results)) {
            $this->output->error('No records found.');
        } else {
            $result = array_pop($results);
            $this->output->success('Bestselling vehicle for a 3 months period is '.$result->model_id.' (between '.$result->from_date.' and '.$result->to_date);
        }
    }
}
