<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    protected $fillable = ['vehicle_id', 'seller_id', 'buyer_id', 'model_id', 'sale_date', 'buy_date'];

    public function buyer()
    {
        return $this->belongsTo('App\Buyer');
    }

}
