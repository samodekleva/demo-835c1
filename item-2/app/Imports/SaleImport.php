<?php
namespace App\Imports;

use App\Sale;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Concerns\Importable;

class SaleImport implements ToModel, WithBatchInserts, WithChunkReading, WithHeadingRow, withProgressBar
{
    use Importable;

    public function model(array $row)
    {

        return new Sale([
            'vehicle_id' => $row['vehicleid'],
            'seller_id' => $row['inhousesellerid'],
            'buyer_id' => $row['buyerid'],
            'model_id' => $row['modelid'],
            'sale_date' => $row['saledate'],
            'buy_date' => $row['buydate'],
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
