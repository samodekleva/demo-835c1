<?php

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;


class Log {

    private $files = [];
    private $file;
    const FILE_STATUS_SYNCED = 'synced';
    const FILE_STATUS_REMOTE = 'on-amazon';
    const FILE_STATUS_LOCAL = 'on-disk';


    function  __construct ($name) {
        $this->file = $name;
        if (is_file($this->file)) {
            $resource = fopen($this->file, 'r');
            if ($resource) {
                $this->load($resource);
            }
            fclose($resource);
        }
    }

    private function load ($handle) {
        while (($buffer = fgets($handle)) !== false) {
            $data = explode('::', trim($buffer));
            $this->files[basename($data[0])] = $data[1];
        }
    }

    public function update ($filename, $state) {
        $filename = basename($filename);
        if (isset($this->files[$filename])) {
            if ($this->files[$filename] == self::FILE_STATUS_SYNCED) return true;
            if ($this->files[$filename] != $state) {
                $this->files[$filename] = self::FILE_STATUS_SYNCED;
                return true;
            }
        }
        $this->files[$filename] = $state;
        return true;
    }

    public function set ($filename, $state) {
        $filename = basename($filename);
        $this->files[$filename] = $state;
        return true;
    }

    public function write() {
        $resource = fopen($this->file, 'w');
        foreach ($this->files as $file => $state) {
            fwrite($resource, $file .'::'. $state . "\n");
        }
        fclose($resource);
    }

    public function get($state) {
        return array_filter($this->files, function ($file) use ($state) {
            return ($state == $file);
        });
    }

    function listContents () {

    }



}