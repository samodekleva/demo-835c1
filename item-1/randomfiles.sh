#!/usr/bin/env bash
dir=$1
mkdir -p $dir
i=0
while [ $i -lt 10 ]
do
  timestamp=$(date -d "$((RANDOM%2+2018))-$((RANDOM%12+1))-$((RANDOM%28+1)) $((RANDOM%23+1)):$((RANDOM%59+1)):$((RANDOM%59+1))" '+%Y%m%d')
  echo $dir/changes-$timestamp.sql
  touch $dir/changes-$timestamp.sql
  i=$(( $i + 1 ))
done

