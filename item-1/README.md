# Sync

Create some random files:

```
./randomfiles.sh <bucket-name-directory-name>
```

Install PHP packages:

```
    composer install
```

Edit AWS credentials

```
    cp config.php.dist config.php
    vim config.php
```

Happy syncing!

```
    php sync.php <bucket-name-directory-name>
```

# Database Updates

Create sqlite database:
```
    touch database.sqlite
```


Migrate:
```
    php transaction.php <directory-name>
```