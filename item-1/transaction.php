<?php

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('database.sqlite');
    }
}



if (!isset($argv[1])) {
    echo("No directory name for the scripts specified.\n\nUsage:\n\tphp transaction.php <directory-name>\n");
    die();
} else {
    $bucket = $argv[1];
}

require __DIR__ . '/vendor/autoload.php';


$local_filesystem = new League\Flysystem\Adapter\Local($bucket);

$files = $local_filesystem->listContents();
$filenames = array_map(function($file) {
    return ($file["path"]);
}, $files);
sort($filenames);

$db = new MyDB();
$db->enableExceptions(true);
$db->exec('BEGIN TRANSACTION');
try {
    foreach ($filenames as $f) {
        $file = $local_filesystem->read($f, "");
        $db->exec($file["contents"]);
    }
} catch (Exception $e) {
    $message = $e->getMessage();
    echo($message."\n");
    $db->exec('ROLLBACK');
    die();
}
$db->exec('COMMIT');

$db->close();