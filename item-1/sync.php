<?php

require 'config.php';

if (!(defined('AWS_KEY') && defined('AWS_SECRET'))) {
    echo ("Please read README.md.\n");
    die();
}

if (!isset($argv[1])) {
    echo ("No directory / bucket name specified.\n\nUsage:\n\tphp sync.php <directory/bucket name>\n");
    die();
} else {
    $bucket = $argv[1];
}
require __DIR__ . '/vendor/autoload.php';
require 'Log.php';

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AdapterInterface;

function upload($from, $to) {
    $from_files = $from->listContents();
    $to_files = $to->listContents();
    $diff = array_udiff($from_files, $to_files, function($a, $b) {
        if ($a["path"] == $b["path"]) {
            return 0;
        } else {
            if ($a["path"] > $b["path"]) {
                return 1;
            } else {
                return -1;
            }
        }
    });
    foreach($diff as $file) {
        $stream = $from->readStream($file["path"]);
        if (!$stream) {
            echo 'Skipping '.$file["path"];
            break;
        }
        $to->writeStream($file["path"], $stream["stream"], new League\Flysystem\Config(['visibility' => AdapterInterface::VISIBILITY_PRIVATE]));
    }
}

function moveFiles($from, $to, $state, $log) {
    $files = array_keys($log->get($state));
    foreach($files as $file) {
        $stream = $from->readStream($file);
        if (!$stream) {
            echo 'Skipping '.$file;
            break;
        }
        $to->writeStream($file, $stream["stream"], ['visibility' => AdapterInterface::VISIBILITY_PRIVATE]);
        $log->update($file, Log::FILE_STATUS_SYNCED);
    }
}


function updateLog($filesystem, $log, $status) {
    $contents = $filesystem->listContents();
    foreach ($contents as $item) {
        if ($item["type"]=="file") {
            $log->update($item["path"], $status);
        }
    }
}

$client = new S3Client([
    'credentials' => [
        'key'    => AWS_KEY,
        'secret' => AWS_SECRET,
    ],
    'region' => 'eu-central-1',
    'version' => 'latest',
]);

$aws_adapter = new AwsS3Adapter($client, $bucket);
$aws_filesystem = new Filesystem($aws_adapter);
$local_filesystem = new Local($bucket);

// Most elegant solution - Direct Sync
// upload($local_filesystem, $aws_adapter);
// upload($aws_adapter, $local_filesystem);


// The compare to log solution
$log = new Log('log.txt');

updateLog($local_filesystem, $log, Log::FILE_STATUS_LOCAL);
updateLog($aws_filesystem, $log, Log::FILE_STATUS_REMOTE);

moveFiles($local_filesystem, $aws_filesystem, Log::FILE_STATUS_LOCAL, $log);
moveFiles($aws_filesystem, $local_filesystem, Log::FILE_STATUS_REMOTE, $log);

$log->write();


